reate table Roles
(
	RoleID int identity primary key,
	RoleName sysname
)

create table Departments
(
	DepartmentID int identity primary key,
	DepartmentName nvarchar(32)
)

create table Persons
(
	PersonID int identity primary key,
	IK char(11) unique,
	FirstName nvarchar(32),
	LastName nvarchar(32),
	Email sysname unique,
	DepartmentID int references Departments(DepartmentID)
)

create table Expenses
(
	ExpenseID int identity primary key,
	ExpenseName nvarchar(32)
)

create table Travels
(
	TravelID int identity primary key,
	PersonID int references Persons(PersonID),
	Place nvarchar(32),
	Description nvarchar(200),
	StartDate DateTime,
	EndDate DateTime,
	Confirmed bit default(0)

)

create table TravelExpenses
(
	TravelexpenseID int identity primary key,
	TravleID int references Travels(TravelID),
	ExpenseID int references Expenses(ExpenseID),
	Duration int default (0), -- stay duration for hotels
	Cost decimal(9,2), -- expense cost for that line
	Confirmed bit default(0)
)
