﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelHen_Auth.Models
{
    public class Vahendid
    {
        public static int DaysBetween(DateTime d1, DateTime d2)
        {
            TimeSpan span = d2.Subtract(d1);
            return (int)span.TotalDays;
        }
        public static string FullNamer(string lastName, string firstName)
        {
            string FullName = $"{lastName}, {firstName}";
            return FullName;
        }
    }
}