﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace TravelHen_Auth.Models
{
    // Example Audit Class
    public class Audit
    {
        // Audit Properties
        public Guid AuditID { get; set; }
        public string UserName { get; set; }
        public string IPAddress { get; set; }
        public string AreaAccessed { get; set; }
        public DateTime TimeAccessed { get; set; }

        // Default Constructor
        public Audit() { }
    }
    // Example AuditingContext 
    public class AuditingContext : DbContext
    {
        public DbSet<Audit> AuditRecords { get; set; }
    }
  
}