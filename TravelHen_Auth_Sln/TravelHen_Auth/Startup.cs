﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TravelHen_Auth.Startup))]
namespace TravelHen_Auth
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
