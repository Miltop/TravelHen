//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
//muutusevältija2
namespace TravelHen_Auth
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Travel
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Travel()
        {
            this.TravelExpenses = new HashSet<TravelExpens>();
        }
        public int TravelID { get; set; }

        [Required()]
        public Nullable<int> PersonID { get; set; }
        
        [Required()]
        public string Place { get; set; }

        [Required()]
        public string Description { get; set; }

        [Required()]
        [Display(Name = "Start date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> StartDate { get; set; }

        [Required()]
        [Display(Name = "End date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> EndDate { get; set; }

        [Display(Name = "Confirmed by Manager")]
        public Nullable<bool> Confirmed { get; set; }

        public Nullable<int> DocumentID { get; set; }

        public string State { get; set; }
    
        public virtual Person Person { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TravelExpens> TravelExpenses { get; set; }
        public virtual File File { get; set; }
    }
}
