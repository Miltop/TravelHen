﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using TravelHen_Auth.Models;

namespace TravelHen_Auth.Controllers
{
    public class HomeController : Controller
    {
        private travelhenEntities db = new travelhenEntities();
        
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                Person CurrentUser = TravelHen_Auth.Person.GetByEmail(User.Identity.GetUserName());
                try
                {
                   
                    if (CurrentUser == null) return View("../Home/Index");
                    ViewBag.UserTravels = from e in db.Travels select e;
                    var testAeg = DateTime.Now.AddMonths(-6);

                    if (CurrentUser.IsInRole("Buchhalter"))
                    {
                        ViewBag.UserTravels = db.Travels.Where(y => y.StartDate > testAeg).Take(20);
                    }
                    else if (CurrentUser.IsInRole("Manager"))
                    { 
                        ViewBag.UserTravels = db.Travels.Where(x => x.Person.DepartmentID == CurrentUser.DepartmentID).Where(y => y.StartDate > testAeg).Take(20);
                    }
                    else
                    {
                        ViewBag.UserTravels = db.Travels.Where(x => x.PersonID == CurrentUser.PersonID).Where(y => y.StartDate > testAeg).Take(20);
                    }
                }
                catch
                {
                    return View();
                }
            }
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "TravelHen is the BEST app to manage your employees and their business trips.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Contact us – if you dare! ";

            return View();
        }
    }
}