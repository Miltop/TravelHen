﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TravelHen_Auth;
using PagedList;
using System.IO;
using Microsoft.AspNet.Identity;

namespace TravelHen_Auth
{
    public class AdminAuthorize : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext == null) throw new ArgumentNullException("httpContext");

            // Make sure the user is authenticated.
            if (httpContext.User.Identity.IsAuthenticated == false) return false;

            // Do you own custom stuff here


            Person u = Person.GetByEmail(HttpContext.Current.User.Identity.Name);
            if (u == null) return false;

            // kas on admin kasutaja

            //bool allow = u.IsInRole("Admin");
            bool allow = false;
            if (u.IsInRole("Admin") || u.IsInRole("Buchhalter")) allow = true;
            return allow;
        }
        public class AccountantAuthorize : AuthorizeAttribute
        {
            protected override bool AuthorizeCore(HttpContextBase httpContext)
            {
                if (httpContext == null) throw new ArgumentNullException("httpContext");

                // Make sure the user is authenticated.
                if (httpContext.User.Identity.IsAuthenticated == false) return false;

                // Do you own custom stuff here


                Person u = Person.GetByEmail(HttpContext.Current.User.Identity.Name);
                if (u == null) return false;

                // kas on admin kasutaja
                bool allow = false;
                if (u.IsInRole("Admin") || u.IsInRole("Buchhalter")) allow = true;

                return allow;
            }
        }
    }
    partial class Person
    {
    // tegin andmebaasi static, sest siis on igas meetodis andmebaas kasutada
    static travelhenEntities db = new travelhenEntities();

    //staatiline meetod, mis leiab Personi tema meili alusel



    public static Person GetByEmail(string email)
    => db.Persons.Where(x => x.Email == email).SingleOrDefault();

    public static Person GetByID(int id)
    => db.Persons.Where(x => x.PersonID == id).SingleOrDefault();


        public bool IsInRole(string roleName)
            => this.PersonRoles
                .Select(x => x.Role.RoleName.ToLower())
                .Contains(roleName.ToLower());
     public string FullName => $"{FirstName} {LastName}";

    }

        
}



namespace TravelHen_Auth.Controllers
{
    public class PeopleController : Controller
    {
    
        private travelhenEntities db = new travelhenEntities();


        // GET: People
        public ActionResult Index(string sortOrder, string searchString, int? page)
        {
            int pageSize = 10;
            int pageNumber = (page ?? 1);

            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");

            //kas on meie kasutaja
            Person u = TravelHen_Auth.Person.GetByEmail(User.Identity.Name);
            if (u == null) return RedirectToAction("Index", "Home");

            ViewBag.CurrentUser = u;

            {
                ViewBag.Isikukoodsort = sortOrder == "IK" ? "ik_desc" : "IK";
                ViewBag.FirstNameSort = sortOrder == "firstName" ? "firstName_desc" : "firstName";
                ViewBag.LastNameSort = sortOrder == "lastName" ? "lastName_desc" : "lastName";
                ViewBag.EmailSort = sortOrder == "email" ? "email_desc" : "email";
                ViewBag.DepartmentNameSort = sortOrder == "departmentName" ? "departmentName_desc" : "departmentName";

                var person = from e in db.Persons select e;
                Person CurrentUser = TravelHen_Auth.Person.GetByEmail(HttpContext.User.Identity.Name);
                if (CurrentUser.IsInRole("Buchhalter")/* || CurrentUser.IsInRole("Admin")*/)
                {
                    person = from e in db.Persons select e;
                }
                else if (CurrentUser.IsInRole("Manager"))
                {
                    person = from e in db.Persons
                              where (e.DepartmentID == CurrentUser.DepartmentID)
                              select e;
                }
                else
                {
                    person = from e in db.Persons where (e.PersonID == CurrentUser.PersonID) select e;
                }
                if (!String.IsNullOrEmpty(searchString))
                {
                    person = person.Where(e => e.LastName.Contains(searchString)
                                           || e.FirstName.Contains(searchString)
                                           || e.IK.Contains(searchString)
                                           || e.Email.Contains(searchString)
                                           || e.Department.DepartmentName.Contains(searchString)
                                           );
                }
                switch (sortOrder)
                {
                    case "lastName":
                        person = person.OrderBy(e => e.LastName);
                        break;
                    case "lastName_desc":
                        person = person.OrderByDescending(e => e.LastName);
                        break;
                    case "firstName":
                        person = person.OrderBy(e => e.FirstName);
                        break;
                    case "firstName_desc":
                        person = person.OrderByDescending(e => e.FirstName);
                        break;
                    case "IK":
                        person = person.OrderBy(e => e.IK);
                        break;
                    case "ik_desc":
                        person = person.OrderByDescending(e => e.IK);
                        break;
                    case "email":
                        person = person.OrderBy(e => e.Email);
                        break;
                    case "email_desc":
                        person = person.OrderByDescending(e => e.Email);
                        break;
                    case "departmentName":
                        person = person.OrderBy(e => e.Department.DepartmentName);
                        break;
                    case "departmentName_desc":
                        person = person.OrderByDescending(e => e.Department.DepartmentName);
                        break;

                    default:
                        person = person.OrderBy(e => e.LastName.Contains(searchString));
                        break;
                }
                return View(person.ToList().ToPagedList(pageNumber, pageSize));
            }

            //return View(db.Persons.ToList());
        }

        // GET: People/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.Persons.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }

            //Gert ei saa muuta Martti pilti
            Person martti = db.Persons.Find(5);
            martti.PictureID = 59;
            db.SaveChanges();
            //Askol on ka alati õige pilt
            Person asko = db.Persons.Find(4);
            asko.PictureID = 61;
            db.SaveChanges();
            // siia pistan paar kontrolli:
            // 1. Kas kasutaja on sisse loginud ja meie kasutaja
            // 2. kas kasutaja kuulub õigesse rolli

            // kas on sisse loginud (või häkker)
            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");

            // kas on meie kasutaja
            Person u = TravelHen_Auth.Person.GetByEmail(User.Identity.Name);
            if (u == null) return RedirectToAction("Index", "Home");
            
            return View(person);


           
        }

        // GET: People/Create
        public ActionResult Create()
        {
            ViewBag.DepartmentID = new SelectList(db.Departments, "DepartmentID", "DepartmentName");
            return View();
        }

        // POST: People/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PersonID,IK,FirstName,LastName,Email,DepartmentID,PictureID")] Person person, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                db.Persons.Add(person);

                //faili lisamine
                if (file != null)
                {
                    if (file.ContentLength > 0)
                    {


                        string[] filenames = file.FileName.Split('\\');
                        File f = new File();
                        f.Filename = filenames[filenames.Length - 1];

                        //if (f.Filetype?.Trim() == "")
                        {
                            string[] filetyps = file.FileName.Split('.');
                            if (filetyps.Length < 2) f.Filetype = "xxx";
                            else f.Filetype = filetyps[filetyps.Length - 1];
                        }
                        f.Contenttype = file.ContentType;

                        if (file.ContentLength > 0)
                        {
                            using (BinaryReader br = new BinaryReader(file.InputStream))
                            {
                                byte[] buff = br.ReadBytes(file.ContentLength);
                                f.Content = buff;
                            }

                            db.Files.Add(f);

                            person.PictureID = f.FileID;
                        }
                    }
                }

                Person CurrentUser = TravelHen_Auth.Person.GetByEmail(HttpContext.User.Identity.Name);
                db.Logs.Add(new Log { UserID = CurrentUser.PersonID, TimeAccessed = DateTime.Now, LOGDESC = $"Lisas uue inimeses nimega: {person.FullName}" });
                db.SaveChanges();

                return RedirectToAction("Index");
            }

            ViewBag.DepartmentID = new SelectList(db.Departments, "DepartmentID", "DepartmentName", person.DepartmentID);
            return View(person);
        }

        // GET: People/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.Persons.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");

            // kas on meie kasutaja
            Person u = TravelHen_Auth.Person.GetByEmail(User.Identity.Name);
            if (u == null) return RedirectToAction("Index", "Home");

            ViewBag.Roles =
            db.Roles.Where(r => !r.PersonRoles
                                  .Select(y => y.PersonID)
                                  .Contains(person.PersonID))
                                  .ToList();

            ViewBag.DepartmentID = new SelectList(db.Departments, "DepartmentID", "DepartmentName", person.DepartmentID);

            if (u.IsInRole("Admin"))
                return View(person);
            if (u.PersonID == person.PersonID)
                return View(person);

            return RedirectToAction("Index");



            
        }

        // POST: People/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PersonID,IK,FirstName,LastName,Email,DepartmentID,PictureID")] Person person, HttpPostedFileBase file)
        {

            if (ModelState.IsValid)
            {
                db.Entry(person).State = EntityState.Modified;

                //faili lisamine
                if (file != null)
                {
                    if (file.ContentLength > 0)
                    {
                        string[] filenames = file.FileName.Split('\\');
                        File f = new File();
                        f.Filename = filenames[filenames.Length - 1];

                        //if (f.Filetype?.Trim() == "")
                        {
                            string[] filetyps = file.FileName.Split('.');
                            if (filetyps.Length < 2) f.Filetype = "xxx";
                            else f.Filetype = filetyps[filetyps.Length - 1];
                        }
                        f.Contenttype = file.ContentType;

                        if (file.ContentLength > 0)
                        {
                            using (BinaryReader br = new BinaryReader(file.InputStream))
                            {
                                byte[] buff = br.ReadBytes(file.ContentLength);
                                f.Content = buff;
                            }

                            db.Files.Add(f);

                            person.PictureID = f.FileID;
                        }
                    }
                    
                 }

                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DepartmentID = new SelectList(db.Departments, "DepartmentID", "DepartmentName", person.DepartmentID);
            return View(person);
        }

        // GET: People/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.Persons.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: People/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Person person = db.Persons.Find(id);
            db.Persons.Remove(person);
            if (person.PictureID != null)
            {
                File file = person.File;
                db.Files.Remove(file);
            }
            Person CurrentUser = TravelHen_Auth.Person.GetByEmail(HttpContext.User.Identity.Name);
            db.Logs.Add(new Log { UserID = CurrentUser.PersonID, TimeAccessed = DateTime.Now, LOGDESC = $"Kustutas inimeses nimega: {person.FullName}" });
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult AddRole(int? id)
        {
            if (!id.HasValue) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            int kasutajaId = id.Value / 1000;
            int rolliID = id.Value % 1000;

            db.PersonRoles.Add(new PersonRole { PersonID = kasutajaId, RoleID = rolliID });
            db.SaveChanges();
            return RedirectToAction("Edit", new { id = kasutajaId });
        }

        public ActionResult RemoveRole(int? id)
        {
            if (!id.HasValue) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            int kasutajaId = id.Value / 1000;
            int rolliID = id.Value % 1000;

            var ur = db.PersonRoles
                .Where(x => x.PersonID == kasutajaId && x.RoleID == rolliID)
                .FirstOrDefault();
            if (ur != null)
            {
                db.PersonRoles.Remove(ur);
                db.SaveChanges();
            }

            db.SaveChanges();
            return RedirectToAction("Edit", new { id = kasutajaId });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
