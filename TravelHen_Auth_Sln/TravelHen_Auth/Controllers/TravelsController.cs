﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using w = TravelHen_Auth;
using TravelHen_Auth.Models;
using System.IO;
using PagedList;
using Microsoft.AspNet.Identity;


namespace TravelHen_Auth.Controllers
{
    [Authorize]
    public class TravelsController : Controller
    {
        private travelhenEntities db = new travelhenEntities();



        // GET: Travels
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {

            //Tabeli lehekülje numbrid
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            // Siin on info, et lehekülje vahetamisel jääks otsingu info ja sorteerimise info alles.
            ViewBag.CurrentSort = sortOrder;
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            // Kontrollime rolli ja kuvame vastava sisu- Managerile tema departmenti travelid, Buchhalterile kõik, Töötajale enda omad
            Person CurrentUser = TravelHen_Auth.Person.GetByEmail(HttpContext.User.Identity.Name);
            //db.Logs.Add(new Log { UserID = CurrentUser.PersonID, TimeAccessed = DateTime.Now, LOGDESC = $"Vaatas Travel kirjeid" });
            //db.SaveChanges();
            if (CurrentUser == null) return View("../Home/Index");
            var travels = from e in db.Travels select e;

            if (CurrentUser.IsInRole("Buchhalter") || CurrentUser.IsInRole("Admin"))
            {
                travels = from e in db.Travels select e;
            }
            else if (CurrentUser.IsInRole("Manager"))
            {
                travels = from e in db.Travels
                          where (e.Person.DepartmentID == CurrentUser.DepartmentID
                          && (e.State == "s" || e.Person.PersonID == CurrentUser.PersonID))
                          select e;
            }
            else
            {
                travels = from e in db.Travels where (e.PersonID == CurrentUser.PersonID) select e;
            }


            Dictionary<int, bool?> proovime = new Dictionary<int, bool?>();
            foreach (TravelExpens kulu in db.TravelExpenses)
            {
                int id = kulu.TravleID ?? -1;
                bool? kinnitus = kulu.Confirmed;
                if (!proovime.ContainsKey(id))
                {
                    proovime.Add(id, kinnitus);
                }
            }
            ViewBag.ConFinder = proovime;

            //Reiside sorteerimiseks
            ViewBag.PlaceSort = sortOrder == "place" ? "place_desc" : "place";
            ViewBag.DescriptionSort = sortOrder == "description" ? "description_desc" : "description";
            ViewBag.StartDateSort = sortOrder == "startdate" ? "startdate_desc" : "startdate";
            ViewBag.EndDateSort = sortOrder == "enddate" ? "enddate_desc" : "enddate";
            ViewBag.ConfirmedSort = sortOrder == "confirmed_desc" ? "confirmed" : "confirmed_desc";
            ViewBag.NameSort = sortOrder == "name" ? "name_desc" : "name";
            ViewBag.ReimbursedSort = sortOrder == "imb" ? "imb_desc" : "imb";


            if (!String.IsNullOrEmpty(searchString))
            {
                travels = travels.Where(e => e.Person.LastName.Contains(searchString)
                                       || e.Person.FirstName.Contains(searchString)
                                       || e.Person.IK.Contains(searchString)
                                       || e.Place.Contains(searchString)
                                       || e.Description.Contains(searchString)
                                       );
            }
            switch (sortOrder)
            {
                case "name":
                    travels = travels.OrderBy(e => e.Person.LastName);
                    break;
                case "name_desc":
                    travels = travels.OrderByDescending(e => e.Person.LastName);
                    break;
                case "place":
                    travels = travels.OrderBy(e => e.Place);
                    break;
                case "place_desc":
                    travels = travels.OrderByDescending(e => e.Place);
                    break;
                case "description":
                    travels = travels.OrderBy(e => e.Description);
                    break;
                case "description_desc":
                    travels = travels.OrderByDescending(e => e.Description);
                    break;
                case "startdate":
                    travels = travels.OrderBy(e => e.StartDate);
                    break;
                case "startdate_desc":
                    travels = travels.OrderByDescending(e => e.StartDate);
                    break;
                case "enddate":
                    travels = travels.OrderBy(e => e.EndDate);
                    break;
                case "enddate_desc":
                    travels = travels.OrderByDescending(e => e.EndDate);
                    break;
                case "confirmed":
                    travels = travels.OrderBy(e => e.Confirmed);
                    break;
                case "confirmed_desc":
                    travels = travels.OrderByDescending(e => e.Confirmed);
                    break;
                default:
                    travels = travels.OrderBy(e => e.Person.LastName.Contains(searchString));
                    break;
            }

            //reisikulude staatuse kuvamine
            return View(travels.ToList().ToPagedList(pageNumber, pageSize));
        }

        // GET: Travels/Details/5
        public ActionResult Details(int id = 0)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Travel travel = db.Travels.Find(id);

            if (travel == null)
            {
                return HttpNotFound();
            }

            //Lisab travelDetails vaatele ligipääsu kõigile kuludele, mis on selle konkreetse reisiga seotud
            ViewBag.TravelExpenses = db.TravelExpenses
                .Where(x => x.TravleID == travel.TravelID)
                .OrderBy(e => e.ExpenseID)
                .ToList();
            return View(travel);
        }

        // GET: Travels/Create
        public ActionResult Create()
        {
            //ViewBag.PersonID = new SelectList(db.Persons, "PersonID", "IK");
            Person CurrentUser = TravelHen_Auth.Person.GetByEmail(User.Identity.GetUserName());
            try
            {
                if (CurrentUser.IsInRole("Buchhalter") || CurrentUser.IsInRole("Admin"))
                {
                    ViewBag.PersonID = new SelectList(db.Persons, "PersonID", "IK");
                }
                else if (CurrentUser.IsInRole("Manager"))
                {
                    int selector = CurrentUser.DepartmentID ?? -1;
                    var employees = db.Persons.Where(x => x.DepartmentID == CurrentUser.DepartmentID);
                    ViewBag.PersonID = new SelectList(employees, "PersonID", "IK");
                }
                else
                {
                    ViewBag.PersonID = new SelectList(db.Persons, "PersonID", "IK");
                }
            }
            catch
            {
                return View();
            }
            return View();
        }

        // POST: Travels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TravelID,PersonID,Place,Description,StartDate,EndDate,Confirmed")] Travel travel)
        {
            Person CurrentUser = TravelHen_Auth.Person.GetByEmail(User.Identity.GetUserName());
            try
            {
                if (CurrentUser.IsInRole("Buchhalter") || CurrentUser.IsInRole("Admin"))
                {
                    ViewBag.PersonID = new SelectList(db.Persons, "PersonID", "IK");
                }
                else if (CurrentUser.IsInRole("Manager"))
                {
                    int selector = CurrentUser.DepartmentID ?? 1;
                    var employees = db.Persons.Where(x => x.DepartmentID == CurrentUser.DepartmentID);
                    ViewBag.PersonID = new SelectList(employees, "PersonID", "IK");
                }
                else
                {
                    ViewBag.PersonID = new SelectList(db.Persons, "PersonID", "IK");
                }
            }
            catch
            {
                return View();
            }

            if (ModelState.IsValid)
            {
                db.Travels.Add(travel);

                if (travel.StartDate > travel.EndDate)
                {
                    ViewBag.ErrorMessage = "The start date can not be after the end date.";
                    return View(travel);
                }

                //päevaraha automaatne lisamine
                int allowancePerDay = 50; //€
                int days = Vahendid.DaysBetween((travel.StartDate ?? System.DateTime.Today), (travel.EndDate ?? DateTime.Today)) + 1;
                int allowance = allowancePerDay * days;
                TravelExpens uus = new TravelExpens
                {
                    TravleID = travel.TravelID,
                    ExpenseID = 3, //allowance
                    Duration = days,
                    Cost = allowance
                };
                db.TravelExpenses.Add(uus);

                db.SaveChanges();
                db.Logs.Add(new Log { UserID = CurrentUser.PersonID, TimeAccessed = DateTime.Now, LOGDESC = $"Lisas uue Traveli Mille ID on: {travel.TravelID}" });
                db.SaveChanges();

                return RedirectToAction("Index");
            }

            return View(travel);
        }

        // GET: Travels/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Travel travel = db.Travels.Find(id);


            if (travel == null)
            {
                return HttpNotFound();
            }
            ViewBag.PersonID = new SelectList(db.Persons, "PersonID", "IK", travel.PersonID);

            //Lisab travelDetails vaatele ligipääsu kõigile kuludele, mis on selle konkreetse reisiga seotud
            ViewBag.TravelExpenses = db.TravelExpenses
                .Where(x => x.TravleID == travel.TravelID)
                .OrderBy(e => e.ExpenseID)
                .ToList();
            return View(travel);
        }

        // POST: Travels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TravelID,PersonID,Place,Description,StartDate,EndDate,Confirmed")] Travel travel, HttpPostedFileBase file)
        {
            //Lisab travelDetails vaatele ligipääsu kõigile kuludele, mis on selle konkreetse reisiga seotud
            ViewBag.TravelExpenses = db.TravelExpenses
                .Where(x => x.TravleID == travel.TravelID)
                .OrderBy(e => e.ExpenseID)
                .ToList();

            ViewBag.PersonID = new SelectList(db.Persons, "PersonID", "IK", travel.PersonID);


            if (ModelState.IsValid)
            {
                ViewBag.PersonID = new SelectList(db.Persons, "PersonID", "IK", travel.PersonID);
                db.Entry(travel).State = EntityState.Modified;

                //kas uus kuupäev on korektne - lõppebb pärast algust
                if (travel.StartDate > travel.EndDate)
                {
                    ViewBag.ErrorMessage = "The start date can not be after the end date.";
                    return View(travel);
                }

                //muudame state ära
                if (travel.Confirmed == true)
                {
                    travel.State = "c";
                }
                if (travel.Confirmed == false)
                {
                    travel.State = "d";
                }


                var oldTravel = db.Travels.AsNoTracking().Where(x => x.TravelID == travel.TravelID).SingleOrDefault(); ;
                //db.Entry(travel).State = EntityState.Modified;
                string logtext = $"EDITED Travel ID: {travel.TravelID}";
                bool logChanged = false;
                string[] propetid = { "PersonID", "Place", "Description", "StartDate", "EndDate" };
                foreach (var p in propetid)
                {
                    var x = db.Entry(travel).Property(p);
                    var y = db.Entry(oldTravel).Property(p);

                    if (!x.CurrentValue.Equals(y.CurrentValue))
                        if (x.CurrentValue != y.CurrentValue)
                        {
                            logtext += $" {p} Old: {y.CurrentValue} New: {x.CurrentValue}";
                            logChanged = true;
                        }
                }
                var cuus = db.Entry(travel).Property("Confirmed");
                var cvana = db.Entry(oldTravel).Property("Confirmed");
                if (cvana.CurrentValue != cuus.CurrentValue)
                {
                    {
                        string before;

                        if (cvana.CurrentValue == null) before = "not reviewd";
                        else if (cvana.CurrentValue.Equals(true)) before = "Approved";
                        else before = "Denied";

                        string after;

                        if (cuus.CurrentValue == null) after = "not reviewd";
                        else if (cuus.CurrentValue.Equals(true)) after = "Approved";
                        else after = "Denied";

                        logtext += $" Confirmed Old: {before} New: {after}";
                        logChanged = true;
                    }
                    if (logChanged) db.Logs.Add(new Log
                    {
                        LOGDESC = logtext,
                        TimeAccessed = DateTime.Now,
                        UserID = Person.GetByEmail(User.Identity.Name).PersonID
                    });

                    db.SaveChanges();
                }

                return View(travel);
            }
            return View(travel);
        }


        // GET: Travels/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Travel travel = db.Travels.Find(id);
            if (travel == null)
            {
                return HttpNotFound();
            }
            return View(travel);
        }

        // POST: Travels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Travel travel = db.Travels.Find(id);
            db.Travels.Remove(travel);

            //koos reisi kustutamisega kustutatakse ka kõik seotud kulud
            var expenses = db.TravelExpenses
                .Where(x => x.TravleID == travel.TravelID);
            foreach (TravelExpens travelExpense in expenses)
            {
                if (travelExpense.File != null)
                {
                    File file = travelExpense.File;
                    db.Files.Remove(file);
                }
                db.TravelExpenses.Remove(travelExpense);
            }
            //koos reisi kustutamisega kustutatakse ka kõik seotud kulud
            Person CurrentUser = TravelHen_Auth.Person.GetByEmail(HttpContext.User.Identity.Name);
            db.Logs.Add(new Log { UserID = CurrentUser.PersonID, TimeAccessed = DateTime.Now, LOGDESC = $"Kustutas Traveli rea, mille ID on: {travel.TravelID}!" });
            db.SaveChanges();

            db.SaveChanges();
            return RedirectToAction("Index"); //TODO
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Reimburse(int? id, string button)
        {
            Travel travel = db.Travels.Find(id);
            var Reisikulud = db.TravelExpenses
                .Where(x => x.TravleID == travel.TravelID)
                .ToList()
               ;
            foreach (TravelExpens kulu in Reisikulud)
            {
                kulu.Confirmed = true;
                db.Entry(kulu).State = EntityState.Modified;
                db.SaveChanges();
            }

            //muudame state
            travel.State = "f";
            db.Entry(travel).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        //Muudab travel state "sumbitted"
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Submit(int? id, string button)
        {
            Travel travel = db.Travels.Find(id);
            if (travel.State == null || travel.State == "d")
            {
                travel.State = "s";
                db.Entry(travel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        //Kaasajastame ja puhastame andmebaasi
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Modernizer()
        {
            foreach (Travel trip in db.Travels)
            {
                if (trip.Confirmed == true)
                {
                    trip.State = "c";
                    db.Entry(trip).State = EntityState.Modified;
                }
                if (trip.Confirmed == false)
                {
                    trip.State = "d";
                    db.Entry(trip).State = EntityState.Modified;
                }
            }
            foreach (TravelExpens expense in db.TravelExpenses)
            {
                if (expense.Confirmed == true && expense.Travel.State != "f")
                {
                    expense.Travel.State = "f";
                    db.Entry(expense).State = EntityState.Modified;
                }
                if (expense.Confirmed == false)
                {
                    db.TravelExpenses.Remove(expense);
                }
            }
            db.SaveChanges();
            return RedirectToAction("Index");
        }

    }


}



