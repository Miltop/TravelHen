﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TravelHen_Auth;
using System.IO;


namespace TravelHen_Auth.Controllers
{
    [Authorize]
    public class TravelExpensController : Controller
    {
        private travelhenEntities db = new travelhenEntities();
        public static int TravelID;

        // GET: TravelExpens
        public ActionResult Index()
        {
            var travelExpenses = db.TravelExpenses.Include(t => t.Expens).Include(t => t.Travel);
            return View(travelExpenses.ToList());
        }

        // GET: TravelExpens/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TravelExpens travelExpens = db.TravelExpenses.Find(id);
            if (travelExpens == null)
            {
                return HttpNotFound();
            }
            return View(travelExpens);
        }

        // GET: TravelExpens/Create
        public ActionResult Create(int? id)
        {
            ViewBag.ExpenseID = new SelectList(db.Expenses, "ExpenseID", "ExpenseName");
            ViewBag.TravleID = new SelectList(db.Travels, "TravelID", "TravelID");
            TravelID = int.Parse(id.ToString());
            return View();
        }

        // POST: TravelExpens/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TravelexpenseID,TravleID,ExpenseID,Duration,Cost,Confirmed,DocumentID")] TravelExpens travelExpens, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                db.TravelExpenses.Add(travelExpens);
                if (!decimal.TryParse(travelExpens.Cost.ToString(), out decimal cost))
                {
                    ViewBag.ErrorMessageCost = "The cost must be a decimal.";
                    return View(travelExpens);
                }

                travelExpens.TravleID = TravelID;
                db.SaveChanges();

                //failide lisamine
                if (file != null)
                    if (file.ContentLength > 0)
                    {
                        string[] filenames = file.FileName.Split('\\');
                        File loodavFail = new File();
                        loodavFail.Filename = filenames[filenames.Length - 1];
                        {
                            string[] filetyps = file.FileName.Split('.');
                            if (filetyps.Length < 2) loodavFail.Filetype = "xxx";
                            else loodavFail.Filetype = filetyps[filetyps.Length - 1];
                        }
                        loodavFail.Contenttype = file.ContentType;

                        if (file.ContentLength > 0)
                        {
                            using (BinaryReader br = new BinaryReader(file.InputStream))
                            {
                                byte[] buff = br.ReadBytes(file.ContentLength);
                                loodavFail.Content = buff;
                            }

                            db.Files.Add(loodavFail);
                            db.SaveChanges();

                            travelExpens.DocumentID = loodavFail.FileID;
                            db.SaveChanges();
                        }
                    }
                return RedirectToAction("../Travels/Index");
            }

            ViewBag.ExpenseID = new SelectList(db.Expenses, "ExpenseID", "ExpenseName", travelExpens.ExpenseID);
            ViewBag.TravleID = new SelectList(db.Travels, "TravelID", "TravelID", travelExpens.TravleID);
            return View(travelExpens);
        }

        // GET: TravelExpens/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TravelExpens travelExpens = db.TravelExpenses.Find(id);
            if (travelExpens == null)
            {
                return HttpNotFound();
            }
            ViewBag.ExpenseID = new SelectList(db.Expenses, "ExpenseID", "ExpenseName", travelExpens.ExpenseID);
            ViewBag.TravleID = new SelectList(db.Travels, "TravelID", "Place", travelExpens.TravleID);
            return View(travelExpens);
        }

        // POST: TravelExpens/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TravelexpenseID,TravleID,ExpenseID,Duration,Cost,Confirmed,DocumentID")] TravelExpens travelExpens, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                db.Entry(travelExpens).State = EntityState.Modified;
                if (!decimal.TryParse(travelExpens.Cost.ToString(), out decimal cost))
                {
                    ViewBag.ErrorMessageCost = "The cost must be a decimal.";
                    return View(travelExpens);
                }
                db.SaveChanges();
                //faili lisamine
                if (file != null)
                    if (file.ContentLength > 0)
                    {
                        string[] filenames = file.FileName.Split('\\');
                        File f = new File();
                        f.Filename = filenames[filenames.Length - 1];

                        //if (f.Filetype?.Trim() == "")
                        {
                            string[] filetyps = file.FileName.Split('.');
                            if (filetyps.Length < 2) f.Filetype = "xxx";
                            else f.Filetype = filetyps[filetyps.Length - 1];
                        }
                        f.Contenttype = file.ContentType;

                        if (file.ContentLength > 0)
                        {
                            using (BinaryReader br = new BinaryReader(file.InputStream))
                            {
                                byte[] buff = br.ReadBytes(file.ContentLength);
                                f.Content = buff;
                            }

                            db.Files.Add(f);
                            db.SaveChanges();

                            travelExpens.DocumentID = f.FileID;
                            db.SaveChanges();
                        }
                    }

                return RedirectToAction("../Travels/Index");
            }


            ViewBag.ExpenseID = new SelectList(db.Expenses, "ExpenseID", "ExpenseName", travelExpens.ExpenseID);
            ViewBag.TravleID = new SelectList(db.Travels, "TravelID", "Place", travelExpens.TravleID);
            return View(travelExpens);
        }

        // GET: TravelExpens/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TravelExpens travelExpens = db.TravelExpenses.Find(id);
            if (travelExpens == null)
            {
                return HttpNotFound();
            }
            return View(travelExpens);
        }

        // POST: TravelExpens/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TravelExpens travelExpens = db.TravelExpenses.Find(id);
            if (travelExpens.File != null)
            {
                File file = travelExpens.File;
                db.Files.Remove(file);
            }
            db.TravelExpenses.Remove(travelExpens);
            db.SaveChanges();
            return RedirectToAction("../Travels/Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }




        // GET: TravelExpens/SelectFromDB/5
        public ActionResult SelectFromDB(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TravelExpens travelExpens = db.TravelExpenses.Find(id);
            if (travelExpens == null)
            {
                return HttpNotFound();
            }
            ViewBag.ExpenseID = new SelectList(db.Expenses, "ExpenseID", "ExpenseName", travelExpens.ExpenseID);
            ViewBag.TravleID = new SelectList(db.Travels, "TravelID", "Place", travelExpens.TravleID);

            //leian kasutamata failid
            Dictionary<int, int> expensesWithDocuments = new Dictionary<int, int>();
            Dictionary<int, int> employeesWithPictures = new Dictionary<int, int>();
            List<File> unusedFiles = new List<File>();
            foreach (TravelExpens expense in db.TravelExpenses)
            {
                if (expense.DocumentID != null && expense.TravleID != null)
                {
                    int fail = expense.DocumentID ?? -1;
                    int tid = expense.TravleID ?? -1;
                    expensesWithDocuments.Add(fail, tid);
                }
            }
            foreach (Person employee in db.Persons)
            {
                if (employee.PictureID != null)
                {
                    int fail = employee.PictureID ?? -1;
                    int pid = employee.PersonID;
                    employeesWithPictures.Add(fail, pid);
                }
            }
            foreach (File fail in db.Files)
            {
                if (expensesWithDocuments.ContainsKey(fail.FileID) == false && employeesWithPictures.ContainsKey(fail.FileID) == false)
                {
                    unusedFiles.Add(fail);
                }
            }
            ViewBag.UnusedFiles = unusedFiles;


            return View(travelExpens);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SelectFromDB(int? id, FormCollection form)
        {
            TravelExpens kulu = db.TravelExpenses.Find(id);
            string input = form["button"];
            kulu.DocumentID = int.Parse(input);
            db.SaveChanges();
            return RedirectToAction($"../TravelExpens/Details/{id}");
        }

    }
}