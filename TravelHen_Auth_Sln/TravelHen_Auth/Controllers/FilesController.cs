﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TravelHen_Auth;
using System.IO;

namespace TravelHen_Auth.Controllers
{
    [Authorize]
    public class FilesController : Controller
    {
        private travelhenEntities db = new travelhenEntities();

        public ActionResult Content(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            File file = db.Files.Find(id);
            if (file == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return File(file.Content, file.Contenttype);
        }

        //Indexi koostamine
        public ActionResult Index()
        {

            ViewBag.Travels = db.Travels.Select(trip => trip.DocumentID).ToList();
            //kontrollimaks, kas fail on kasutuses
            Dictionary<int, int> expensesWithDocuments = new Dictionary<int, int>();
            Dictionary<int, int> employeesWithPictures = new Dictionary<int, int>();
            foreach (TravelExpens expense in db.TravelExpenses)
            {
                if (expense.DocumentID != null && expense.TravleID != null)
                {
                    int fail = expense.DocumentID ?? -1;
                    int id = expense.TravleID ?? -1;
                    expensesWithDocuments.Add(fail, id);
                }
            }
            foreach (Person employee in db.Persons)
            {
                if (employee.PictureID != null)
                {
                    int fail = employee.PictureID ?? -1;
                    int id = employee.PersonID;
                    employeesWithPictures.Add(fail, id);
                }
            }
            ViewBag.TravelExpenseIDs = expensesWithDocuments;
            ViewBag.EmployeeIDs = employeesWithPictures;

            return View(db.Files.ToList());
        }


        // GET: Files/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            File file = db.Files.Find(id);
            if (file == null)
            {
                return HttpNotFound();
            }
            return View(file);
        }

        // GET: Files/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "FileID")] File fail,HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {

                    string[] filenames = file.FileName.Split('\\');

                    fail.Filename = filenames[filenames.Length-1];

                    {
                        string[] filetyps = file.FileName.Split('.');
                        if (filetyps.Length < 2) fail.Filetype = "xxx";
                        else fail.Filetype = filetyps[filetyps.Length - 1];
                    }
                    fail.Contenttype = file.ContentType;

                    if (file.ContentLength > 0)
                    {
                        using (BinaryReader br = new BinaryReader(file.InputStream))
                        {
                            byte[] buff = br.ReadBytes(file.ContentLength);
                            fail.Content = buff;
                        }
                        db.Files.Add(fail);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                }
            }

            return View(file);
        }


        // GET: Files/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            File file = db.Files.Find(id);
            if (file == null)
            {
                return HttpNotFound();
            }
            return View(file);
        }

        // POST: Files/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "FileID,Filename,Filetype,Contenttype,Content")] File file)
        {
            if (ModelState.IsValid)
            {
                db.Entry(file).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(file);
        }

        // GET: Files/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            File file = db.Files.Find(id);
            if (file == null)
            {
                return HttpNotFound();
            }
            return View(file);
        }

        // POST: Files/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            File file = db.Files.Find(id);
            db.Files.Remove(file);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
