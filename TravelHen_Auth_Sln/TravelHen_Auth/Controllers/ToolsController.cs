﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TravelHen_Auth.Controllers
{   
    public class ToolsController : Controller
    {
        private travelhenEntities db = new travelhenEntities();
        // GET: Tools
        [AdminAuthorize]
        public ActionResult Index()
        {
            ViewBag.Roles = db.Roles.OrderBy(e => e.RoleName).ToList();
            ViewBag.Departments = db.Departments.OrderBy(e => e.DepartmentName).ToList();
            ViewBag.Expenses = db.Expenses.OrderBy(e => e.ExpenseName).ToList();
            return View();
        }
    }
}