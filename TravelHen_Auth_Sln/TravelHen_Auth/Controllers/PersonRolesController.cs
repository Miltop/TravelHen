﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TravelHen_Auth;

namespace TravelHen_Auth.Controllers
{
    public class PersonRolesController : Controller
    {
        private travelhenEntities db = new travelhenEntities();

        //GET: PersonRoles
        public ActionResult Index()
        {
            var personRoles = db.PersonRoles.Include(p => p.Person).Include(p => p.Role);
            return View(personRoles.ToList());
        }

        
        // GET: PersonRoles/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PersonRole personRole = db.PersonRoles.Find(id);
            if (personRole == null)
            {
                return HttpNotFound();
            }
            return View(personRole);
        }

        // GET: PersonRoles/Create
        public ActionResult Create()
        {
            ViewBag.PersonID = new SelectList(db.Persons, "PersonID", "IK");
            ViewBag.RoleID = new SelectList(db.Roles, "RoleID", "RoleName");
            return View();
        }

        // POST: PersonRoles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PersonRoleID,PersonID,RoleID")] PersonRole personRole)
        {
            if (ModelState.IsValid)
            {
                db.PersonRoles.Add(personRole);
                db.SaveChanges();
                return RedirectToAction("../Tools/Index");
            }

            ViewBag.PersonID = new SelectList(db.Persons, "PersonID", "IK", personRole.PersonID);
            ViewBag.RoleID = new SelectList(db.Roles, "RoleID", "RoleName", personRole.RoleID);
            return View(personRole);
        }

        // GET: PersonRoles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PersonRole personRole = db.PersonRoles.Find(id);
            if (personRole == null)
            {
                return HttpNotFound();
            }
            ViewBag.PersonID = new SelectList(db.Persons, "PersonID", "IK", personRole.PersonID);
            ViewBag.RoleID = new SelectList(db.Roles, "RoleID", "RoleName", personRole.RoleID);
            return View(personRole);
        }

        // POST: PersonRoles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PersonRoleID,PersonID,RoleID")] PersonRole personRole)
        {
            if (ModelState.IsValid)
            {
                db.Entry(personRole).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("../Tools/Index");
            }
            ViewBag.PersonID = new SelectList(db.Persons, "PersonID", "IK", personRole.PersonID);
            ViewBag.RoleID = new SelectList(db.Roles, "RoleID", "RoleName", personRole.RoleID);
            return View(personRole);
        }

        // GET: PersonRoles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PersonRole personRole = db.PersonRoles.Find(id);
            if (personRole == null)
            {
                return HttpNotFound();
            }
            return View(personRole);
        }

        // POST: PersonRoles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PersonRole personRole = db.PersonRoles.Find(id);
            db.PersonRoles.Remove(personRole);
            db.SaveChanges();
            return RedirectToAction("../Tools/Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
