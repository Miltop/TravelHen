//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
//muutusevältija2
namespace TravelHen_Auth
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Person
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Person()
        {
            this.Travels = new HashSet<Travel>();
            this.PersonRoles = new HashSet<PersonRole>();
        }
    
        public int PersonID { get; set; }

        [Required()]
        [Display(Name = "National ID number")]
        public string IK { get; set; }

        [Required()]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required()]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Required()]
        public string Email { get; set; }

        [Required()]
        public Nullable<int> DepartmentID { get; set; }

        public Nullable<int> PictureID { get; set; }
    
        public virtual Department Department { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Travel> Travels { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PersonRole> PersonRoles { get; set; }
        public virtual File File { get; set; }
    }
}
